
ORG 0x4000

main:
    call disableFirmware
    call loop

loop:
REPT 10
    halt
ENDM
    call waitVSync
    call dibujarPixel
    jr loop

dibujarPixel:
    ld a, (pixel)
    ld (0xC000), a
    srl a
    jr nc, actualizar
        ld a, 0x88
actualizar:
    ld (pixel), a
    ret

pixel: DB 0x88

INCLUDE "lib/main.asm"

END main
