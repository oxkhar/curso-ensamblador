
ORG 0x4000

posicionMemVideo EQU 0xC0FF
numeroColumnas EQU 10

main:
    call disableFirmware

loop:
REPT 100
    halt
ENDM
    call calculaPosicion
    call waitVSync
    call borrarBala
    call dibujarBala
    jp loop

calculaPosicion:
    ld hl, posicion
    inc (hl)
    ld a, numeroColumnas
    cp (hl)
    jr nz, volvemos
        ld (hl), 0
volvemos:
    ret

borrarBala:
    ld hl, posicionMemVideo
    ld bc, borrado
    ld a, (posicion)
    dec a                  ; tenemos que borrar la posición anterior
    jp p, callDibujarBala
        ld a, numeroColumnas-1 ; Si es la posición primera tendremos que borrar la última
callDibujarBala:
    call dibujarSrite
    ret

dibujarBala:
    ld hl, posicionMemVideo
    ld bc, bala
    ld a, (posicion)
    call dibujarSrite
    ret

; A => Columna horizontal de posición
; HL => Dirección de memoria video a dibujar
; BC => Dirección de memoria de los bytes a dibujar
; Destroy A,HL,BC,DE
dibujarSrite:
    ld d, 0          ; prepara para sumar la posición de la bala
    ld e, a          ;
    add hl, de       ; suma a la dirección de video la posición horizontal

    ld d, 4
otraLinea:
    ld a, (bc)  ; carga el byte a dibujar
    ld (hl), a  ; escribe el byte a dibujar en la dirección de video

    ld a, h      ; Sumamos 0x08 para saltar a la siguiente linea
    add a, 0x08
    ld h, a

    inc bc
    dec d
    ret z
    jr otraLinea

posicion: DB numeroColumnas-1
bala: DB 0x04, 0xE7, 0xE7, 0x04
borrado: DB 0x00, 0x00, 0x00, 0x00

INCLUDE "lib/main.asm"

END main
