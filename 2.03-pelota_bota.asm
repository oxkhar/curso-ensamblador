
ORG 0x4000

videoAddress EQU 0xC000
ballHeight   EQU 4
ballNumMoves EQU 9

main:
    call disableFirmware

loop:
    call ballCalculate
    ld b, 10
    call waitSomeTime
    call ballErase
    call ballDraw
    jr loop


ballCalculate:
    ld hl, ballMoveNum
    ld d, 0             ; Calculate which position of moves
    ld e, (hl)

    ld hl, ballMove     ; Address to list of count of moves
    add hl, de          ; addres of num of moves to read

    ld a, e
    inc a               ; next move
    cp ballNumMoves
    jr nz, saveNumMoves
        ld a, 0
saveNumMoves:
    ld (ballMoveNum), a

    ld a, (hl)          ; calculate next video addres with num of moves
    ld hl, videoAddress
    ld de, 0x0800
addLineLoop:
    cp 0
    jr z, continue
        add hl, de
        dec a
        jr addLineLoop
continue:
    ex de, hl     ; DE => Next address to move, HL => Don't matter

    ld hl, (ballPosNext) ; Next address pass to be previous address
    ld (ballPosPrev), hl

    ld (ballPosNext), de ; At the end write next address to move

    ret

ballErase:
    ld hl, (ballPosPrev)
    ld bc, 0x00
    call drawSprite
    ret

ballDraw:
    ld hl, (ballPosNext)
    ld bc, ball
    jp drawSprite

; HL => Video memory address to start
; BC => Sprite memory to draw (if 0x0000 then erase with 0x00)
; Destroy AF,HL,BC,DE
drawSprite:
    ld d, ballHeight

drawLineLoop:
    ld a, 0x00       ; Address in BC == 0x00** then erase
    or b             ; Check if 0x00
    jr z, drawByte   ; Will draw byte 0x00, so erase previous content
    ld a, (bc)       ; Address in BC is valid load its byte to draw
    inc bc           ; Prepare next address to draw
drawByte:
    ld (hl), a  ; write byte to draw in current video address

    dec d      ; counter of lines decrease
    ret z      ; if all lines are drawed then end routine

    ld a, h      ;
    add a, 0x08  ; To jump next line adding 0x08 to H
    ld h, a      ;

    jr drawLineLoop


ball:          DB 0x66, 0xDB, 0xBD, 0x66
ballMoveNum:   DB 0x00
ballMove:      DB 0x04, 0x02, 0x01, 0x00, 0x00, 0x00, 0x01, 0x02, 0x04
ballPosNext:   DW 0x0000
ballPosPrev:   DW 0x0000

INCLUDE "lib/main.asm"

END main
