
ORG 0x4000

INCLUDE "lib/defs.h.asm"

defPositions roofPosition, 0, 48
defPositions heroPosition, 0, 56
defPositions shotPosition, 2, 56
defPositions shotMovePosition, 3, 58
defPositions explodePosition, 79, 56
defPositions floorPosition, 0, 64

main:
    call disableFirmware

    call drawRoof
    call drawFloor
    call drawHero

loop:
    ld b, 100
    call waitSomeTime

    call shotHero

    call moveShot

    call explode

    jr loop


drawRoof:
    ; Draw roof
    ld hl, roofPosition
    ld (tileVideoStart), hl
    ld de, roofTile
    ld bc, 0x5010
    jp drawTile

floorTileHSize EQU 1   ; Horizontal bytes size
floorTileVLines EQU 4   ; Vertical lines size
floorTileSize EQU floorTileHSize * floorTileVLines   ; Total bytes from each floor tile
drawFloor:
    ; Draw floor
    ld hl, floorPosition
    ld (tilePatternVideo), hl

    ld hl, floorTilePattern
    ld (tilePattern), hl

    ld hl, floorTile
    ld (tileAddress), hl

    ld a, floorTileVLines
    ld (tileLinesSize), a

    ld a, floorTileSize
    ld (tileTotalSize), a

    ld a, 80
    ld (tilesToDraw), a

    jp drawTilePattern

explode:
    ld a, 0
explodeLoop:
    ex af, af'  ;pa'tras
    ld b, 30
    call waitSomeTime
    ex af, af'   ;pa'lante

    ld h, 0
    ld l, a
    sla l
    ld de, explodeSprite
    add hl, de
    ld e, (hl)
    inc hl
    ld d, (hl)

    ld hl, explodePosition
    ld bc, 0x0108
    ex af, af'   ;pa'tras
    call drawSprite

    ex af, af'  ;pa'lante
    inc a
    cp 4
    jr nz, explodeLoop
    ret


moveShot:
    ld a, -75
moveShotLoop:

    ex af, af'  ;pa'tras
    ld b, 1
    call waitSomeTime

    ex af, af'   ;pa'lante
    bit 0, a
    jr z, pair
        ld de, bulletHalf
        jr drawMoveBullet
pair:
        ld de, bullet

drawMoveBullet:
    ld h, 0
    ld l, a
    bit 7, a
    jr z, positive
        ld h, 0xff
positive:
    ld bc, 75
    add hl, bc
    srl h   ; divide por 2 ya que avanza de 4 en 4 pixels
    rr l
    ld bc, shotMovePosition
    add hl, bc
    ld bc, 0x0204
    ex af, af'   ;pa'tras
    call drawSprite

    ex af, af'  ;pa'lante
    inc a
    cp 77
    jr nz, moveShotLoop
    ret

drawHero:
    ld hl, heroPosition
    ld de, heroSprite
    ld bc, 0x0208
    jp drawSprite



spriteAddressHeroShot: DW heroShotSprite
spriteAddressShotStart: DW shotStartSprite
shotHero:
    ld a, 0
    ld hl, heroShotSprite
    ld (spriteAddressHeroShot), hl
    ld hl, shotStartSprite
    ld (spriteAddressShotStart), hl
shotHeroLoop:
    ld hl, (spriteAddressShotStart) ; Next Sprite Shot to draw
    ld e, (hl)
    inc hl
    ld d, (hl)

    ld hl, (spriteAddressShotStart) ; Next Sprite
    inc hl
    inc hl
    ld (spriteAddressShotStart), hl ; Next Sprite save

    ld hl, shotPosition
    ld bc, 0x0208
    ex af, af'         ;pa'tras
    call drawSprite

    ex af, af'         ;pa'lante
    ld b, a
    ex af, af'          ;pa'tras
    ld a, b
    cp 0
    jr z, continueLoop   ; Hero shot start in step 1 so wait firts step
drawHeroShot:
    ld hl, (spriteAddressHeroShot) ; Load in DE address of sprite to draw
    ld e, (hl)
    inc hl
    ld d, (hl)

    ld hl, (spriteAddressHeroShot) ; Next Sprite
    inc hl
    inc hl
    ld (spriteAddressHeroShot), hl

    ld hl, heroPosition
    ld bc, 0x0208
    call drawSprite
continueLoop:
    ld b, 20
    call waitSomeTime
    ex af, af'          ;pa'lante

    inc a
    cp 7  ; Animation has 7 steps, so hero shot start unb step 1
    jr nz, shotHeroLoop

    ret


videoAddressStartSprite: DW 0x0000
SizeParamsSprite: DW 0x0000
; HL => Video memory to start to draw
; DE => Address where sprite is allocated
; B => Bytes horizontal size
; C => Lines verticla size
; Destroy HL, DE, BC, AF
drawSprite:
    ld (SizeParamsSprite), bc
    ld (videoAddressStartSprite), hl
drawSpriteLoop:
    ld a, (de)
    ld (hl), a
    inc de
    inc hl
    djnz drawSpriteLoop

    ld hl, (videoAddressStartSprite)
    ld a, 0x08
    add a, h
    ld h, a

    ld bc, (SizeParamsSprite)
    dec c
    jr nz, drawSprite
    ret


tileVideoStart: DW 0x0000
; tileVideoStart => Video memory to start to draw
; DE => Address where tile is allocated
; B  => Num tiles to draw
; C  => Vertical size (<= 8)
; Destroy AF, BC, DE, HL
drawTile:
    ld a, b
    ex af, af'
    ld hl, (tileVideoStart)
drawTileLoop:
    ex af, af'
    ld b, a
    ex af, af'
    ld a, (de)
drawLine:
    ld (hl), a
    inc hl
    djnz drawLine
drawTineNextLine:
    ld hl, (tileVideoStart)
    call nextLineVideoAddress ; Next line in screen
    ld (tileVideoStart), hl

    inc de   ; Next line in tile

    dec c
    jr nz, drawTileLoop
    ret


tilePatternVideo DW 0x0000
tilePattern DW 0x0000
tilePatternNow DW 0x0000
tileAddress DW 0x0000
tileLinesSize DB 0
tileTotalSize DB 0
tilesToDraw DB 0
; tilePatternVideo => Video memory to start to draw
; tilePattern    => Address where pattern is (ends with -1)
; tileAddress    => Address where tile is allocated
; tileLinesSize  => Vertical size (<= 8)
; Destroy AF, BC, DE, HL
drawTilePattern:
    ld hl, (tilePattern)
    ld (tilePatternNow), hl
drawTilePatternLoop:
    ld hl, (tilePatternNow)
    ld a, (hl)
    xor b
    ld d, a     ; save beore tile readed
readNextPattern:
    ld a, (hl)  ; read current tile in pattern

    cp -1    ; read last element?
    jr z, tilePatternFinish
    cp d     ; read different tile than before?
    jr nz, drawTilePatternNow

    inc b
    inc hl
    jr readNextPattern
tilePatternFinish:
    ld hl, (tilePattern)
drawTilePatternNow:
    ld (tilePatternNow), hl

    ld a, (tilesToDraw)
    sub b
    jp p, okContinue
nosHemosPasado:
        add a, b
        ld b, a
        ld a, 0
okContinue:
    ld (tilesToDraw), a

    ld c, d                ; calculate which tile draw
    ld a, (tileTotalSize)
    sra a
calculateTile:
    sla c
    sra a
    jr nz, calculateTile
    ld a, c

    ld hl, (tilePatternVideo)
    ld (tileVideoStart), hl
    ld d, 0
    ld e, b
    add hl, de
    ld (tilePatternVideo),hl

    ld de, (tileAddress)
    add a, e
    ld e, a
    ld a, d
    adc a, 0
    ld d, a

    ld a, (tileLinesSize)   ; prepare lines of tile to draw
    ld c, a

    call drawTile

    ld a, (tilesToDraw)
    or 0
    jr nz, drawTilePatternLoop

    ret

; Modify HL video address to next line
; HL => Current Address
; Destroy: A
; Returns: HL => New address
nextLineVideoAddress:
    ld a, h      ; [4]
    add a, 0x08  ; [7]
    ld h, a      ; [4]
    ret nc       ; [10] (Better => 25)
    ld a, l      ; [4]
    add a,0x50   ; [7]
    ld l, a      ; [4]
    ld a, 0xc0   ; [7]
    adc a, h     ; [4]
    ld h, a      ; [4] (Worst => 45)
    ret

roofTile:  DB 0xF0,0x5A,0xA5,0x1E,0x0F,0x0A,0x05,0x08
           DB 0x10,0x20,0x30,0x40,0x50,0x60,0x70,0x80

floorTile: DB 0xFF,0xA5,0x5A,0xFF
floorTileSolid: DB 0xFF,0xFF,0xFF,0xFF

floorTilePattern: DB 0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,1,0,1,1,0,0,0,0,0,1,0,0,-1

heroSprite: DB 0x13, 0x10
            DB 0x30, 0xD6
            DB 0x73, 0xB6
            DB 0x30, 0xC0
            DB 0x01, 0x08
            DB 0x33, 0xEE
            DB 0x76, 0x5A
            DB 0x23, 0xA5

heroShotSprite:  DW heroShotSprite1, heroShotSprite2, heroShotSprite3, heroShotSprite2, heroShotSprite1, heroSprite

heroShotSprite1: DB 0x02, 0x20
                 DB 0x30, 0xA4
                 DB 0x72, 0xE4
                 DB 0x30, 0xC0
                 DB 0x01, 0x08
                 DB 0x33, 0xEE
                 DB 0x76, 0x5A
                 DB 0x23, 0xA5
heroShotSprite2: DB 0x04, 0x80
                 DB 0x61, 0x80
                 DB 0xF5, 0x80
                 DB 0x70, 0x00
                 DB 0x03, 0x00
                 DB 0x77, 0xCC
                 DB 0xDE, 0x4A
                 DB 0x65, 0xA4
heroShotSprite3: DB 0x14, 0x00
                 DB 0x52, 0x00
                 DB 0xF6, 0x00
                 DB 0x60, 0x00
                 DB 0x02, 0x00
                 DB 0x77, 0xCC
                 DB 0xED, 0x84
                 DB 0x56, 0x48


bullet: DB 0x00, 0x0C
        DB 0x00, 0x76
        DB 0x00, 0x76
        DB 0x00, 0x0C

bulletHalf: DB 0x03, 0x00
            DB 0x11, 0xC8
            DB 0x11, 0xC8
            DB 0x03, 0x00


explodeSprite: DW explode1, explode2, explode3, explode4
explode1:   DB 0x00
            DB 0x00
            DB 0x00
            DB 0x32
            DB 0x31
            DB 0x00
            DB 0x00
            DB 0x00

explode2:   DB 0x90
            DB 0x40
            DB 0x30
            DB 0x73
            DB 0x73
            DB 0x30
            DB 0x50
            DB 0x40

explode3:   DB 0x00
            DB 0x90
            DB 0x00
            DB 0x80
            DB 0x44
            DB 0x00
            DB 0x11
            DB 0x90

explode4:   DB 0x00
            DB 0x00
            DB 0x00
            DB 0x00
            DB 0x00
            DB 0x00
            DB 0x00
            DB 0x00

shotStartSprite: DW shot1, shot2, shot3, shot4, shot5, shot5, shot6
shot1:
DB 0x80,0x00
DB 0xC8,0x00
DB 0xC8,0x00
DB 0x80,0x00
DB 0x00,0x00
DB 0x00,0x00
DB 0x00,0x00
DB 0x00,0x00

shot2:
DB 0x60,0x00
DB 0xF6,0x00
DB 0xF6,0x00
DB 0x60,0x00
DB 0x00,0x00
DB 0x00,0x00
DB 0x00,0x00
DB 0x00,0x00


shot3:
DB 0x30,0x40
DB 0x73,0x20
DB 0x33,0x00
DB 0x80,0x40
DB 0x40,0x80
DB 0x00,0x00
DB 0x00,0x00
DB 0x00,0x00


shot4:
DB 0x60,0x20
DB 0x81,0x00
DB 0x11,0x88
DB 0x11,0x88
DB 0x01,0x00
DB 0x80,0x80
DB 0x00,0x10
DB 0x40,0x00


shot5:
DB 0x80,0x00
DB 0x00,0x00
DB 0x01,0x08
DB 0x00,0xCC
DB 0x00,0xCC
DB 0x01,0x08
DB 0x80,0x00
DB 0x10,0x00


shot6:
DB 0x00,0x00
DB 0x00,0x00
DB 0x00,0x0C
DB 0x00,0x76
DB 0x00,0x76
DB 0x00,0x0C
DB 0x00,0x00
DB 0x00,0x00



INCLUDE "lib/main.asm"

END main
