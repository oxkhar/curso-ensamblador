
INCLUDE "macros/common.asm"
INCLUDE "macros/draw.asm"
INCLUDE "macros/structures.asm"
INCLUDE "macros/stack.asm"
INCLUDE "macros/entities.asm"

DEBUG DEFL true

ORG 0x4000

init:
    call disableFirmware

main:
    ld iy, roofTile
    ld hl, roofPosition
    ld a, scenarioLenght
    ld b, 0x00
    call drawTile

    ld ix, floorPattern
    ld iy, floorTile
    ld hl, floorPosition
    ld a, scenarioLenght
    call drawPattern

    call createEnemies

    call createTank

    call waitVSync
    halt
loop:

    di
    colorBorder 02
    call updatePlayer
    colorBorder 03
    call updateBullets
    colorBorder 04
    call updateEnemies
    colorBorder 05
    call updateEntities
    ei

    colorBorder 01

    call waitVSync

    colorBorder 01, 3

    di
    colorBorder 10
    call eraseEntitySprites
    colorBorder 11
    call drawEnemies
    colorBorder 12
    call drawBullets
    colorBorder 13
    call drawPlayer
    colorBorder 14
    call drawEntities
    ei

    colorBorder 01, 1

;; Tick, tack...
    ld a, (timer)
    inc a
    cp animateFrequencyScreen
    jr nz, _timerContinue
        xor a
_timerContinue:
    ld (timer), a

    jp loop

; #####
INCLUDE "inc/scenario.asm"
INCLUDE "inc/entities.asm"
INCLUDE "inc/tank.asm"
INCLUDE "inc/shot.asm"
INCLUDE "inc/explosion.asm"
INCLUDE "inc/bullet.asm"
INCLUDE "inc/enemy.asm"

INCLUDE "lib/main.asm"
INCLUDE "lib/draw.asm"
INCLUDE "lib/stack.asm"
INCLUDE "lib/animate.asm"

END 0x4000
