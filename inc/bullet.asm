
stackEntity Bullets, 2

entityStruct bullet, updateBullet, (shotPositionX + 1), (shotPositionY + 2), 0x01, bulletSprite, 0x01

spriteStruct bulletSprite, 0x04, 0x02, 0x02, animateSpriteOnce

createBullet:
    ld ix, bullet

    res entityStatusIsFinished, (ix + entityStatus)

    ld (ix + entityAddress), LOW bulletPosition
    ld (ix + entityAddress + 1), HIGH bulletPosition
    ld (ix + entityPosX), bulletPositionX

    ld hl, stackBullets
    call addEntityInStack

    jp spriteInitAnimation

; IX => Bullet structure
updateBullet:
    call spriteUpdate

    ; Move with sprite animation, not need anymore...
    ld a, (ix + spriteFrame)
    cp lastFrame
    jr nz, _bulletCheckCollision

    ; when finish animation then move sprite
    call spriteInitAnimation

    ;; Update position
    set entityStatusIsDirty, (ix + entityStatus)

    ; move to left
    ld a, (ix + entityAddress)
    add a, 0x01
    ld (ix + entityAddress), a
    ld a, (ix + entityAddress + 1)
    adc a, 0x00
    ld (ix + entityAddress + 1), a

    inc (ix + entityPosX)

_bulletCheckCollision:
    ld b, (ix + entityPosX)
    inc b        ; add +1 to calculate with the end of bullet
    ld c, (ix + entityPosY)

    push ix
    call collisionEnemies  ; return HL with address of collision
    pop ix
    or 0x00  ; it's collision when A is zero
    jr z, _bulletImpact

    ld a, b    ; is impact at the end?
    ld hl, wallPosition
    cp wallPositionX
    ret nz

_bulletImpact:
    ; Bullet impact...
    push ix
    call createExplosion
    pop ix

; IX => Bullet structure
; HL => Address to draw explosion
finishBullet:
    set entityStatusIsFinished, (ix + entityStatus)

    ld hl, stackBullets
    call removeEntityFromStack

    jp finishTankFiring

;;
; Update Entities
; Params: B => Pos X ; C => Pos Y;
; Destroy: HL, DE
; Returns: A => 0 -> Explode, else no explosion; HL => Address of explosion
stackLoopBegin stackEnemies, collisionEnemies

    ld a, b
    sub (ix + entityPosX)
    jp m, _collisionEnemiesLoop

    or 0
    jr z, _bulletCollision

    ld e, (ix + entitySprite)      ;
    ld d, (ix + entitySprite + 1)  ;
    ld iyl, e                      ; IY with sprite structure
    ld iyh, d

    sub (iy + imageWidth)
    jp m, _bulletCollision

stackLoopEnd stackEnemies, collisionEnemies
    ret

_bulletCollision:
    xor 0
    ld l, (ix + entityAddress)
    ld h, (ix + entityAddress + 1)
    set entityStatusIsDestroyed, (ix + entityStatus)
    jr _collisionEnemiesExit


bulletSpriteData:
bulletHalf:
    DB 0x03, 0x00
    DB 0x11, 0xC8
    DB 0x11, 0xC8
    DB 0x03, 0x00
bulletFull:
    DB 0x00, 0x0C
    DB 0x00, 0x76
    DB 0x00, 0x76
    DB 0x00, 0x0C

