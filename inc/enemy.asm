
enemyStatusDisabled EQU 4

enemiesTotal EQU 16
enemySize EQU entitySize

stackEntity Enemies, enemiesTotal

listEnemies:
    entityStruct enemyHead, updateEnemy, (scenarioPositionX + scenarioLenght - 2), (scenarioPositionY + 8), 0x20, enemyHeadSprite, 0x10
    entityStruct enemyNinja, updateEnemy, (scenarioPositionX + scenarioLenght - 2), (scenarioPositionY + 8), 0x20, enemyNinjaSprite, 0x0f
    entityStruct enemyUfo, updateEnemy, (scenarioPositionX + scenarioLenght - 2), (scenarioPositionY + 8), 0x20, enemyUfoSprite, 0x0a
listEnemiesSpace:
    DS enemySize * enemiesTotal, 0x00

spriteEnemies:
    spriteStruct enemyHeadSprite, 0x08, 0x02, 0x02, animateSpriteLoop
    spriteStruct enemyNinjaSprite, 0x08, 0x02, 0x02, animateSpriteLoop
    spriteStruct enemyUfoSprite, 0x08, 0x02, 0x02, animateSpriteLoop

countTotalEnemies: DB 0x00
startPosition: DW 0x0000

createEnemies:
    ld ix, enemyHead
    set enemyStatusDisabled, (ix + entityStatus)
    ld hl, stackEnemies
    call addEntityInStack

    ld ix, enemyNinja
    set enemyStatusDisabled, (ix + entityStatus)
    ld hl, stackEnemies
    call addEntityInStack

    ld ix, enemyUfo
    set enemyStatusDisabled, (ix + entityStatus)
    ld hl, stackEnemies
    call addEntityInStack

;;;
    ld a, enemiesTotal - 3
    ld hl, listEnemies
    ld de, listEnemiesSpace
_createEnemiesLoop:
    ld ixl, e
    ld ixh, d
    ex af, af'
    exx
    ld hl, stackEnemies
    call addEntityInStack
    exx
    ex af, af'

    ld bc, enemySize
    ldir

    dec a
    jr nz, _createEnemiesLoop

    ld ix, enemyHead
    res enemyStatusDisabled, (ix + entityStatus)
    set entityStatusIsDirty, (ix + entityStatus)
    nextFrameInTime entity
    nextFrameInTime sprite
    ld (startPosition), ix

    ret

; IX => Bullet structure
updateEnemy:
    bit entityStatusIsFinished, (ix + entityStatus)
    jp nz, finishEnemy
    bit entityStatusIsDestroyed, (ix + entityStatus)
    jp nz, destroyEnemy
    bit enemyStatusDisabled, (ix + entityStatus)
    jr z, _updateEnemyMove
_enemyStart:
        ld hl, (startPosition)
        ld a, h
        or 0
        ret nz  ; Some enemy is in start position

        res enemyStatusDisabled, (ix + entityStatus)
        set entityStatusIsDirty, (ix + entityStatus)
        nextFrameInTime entity
        nextFrameInTime sprite
        ld (startPosition), ix
        ret
_updateEnemyMove:
    call spriteUpdate ; Returns IY with sprite

    ld a, (ix + entityPosX)    ; Is the middle of scenario?
    cp  scenarioPositionX + scenarioLenght / 3
    ret z

    ; Update position
    ld a, (timer)
    cp (ix + entityFrame) ; Check if it's the time to move
    ret nz
    nextFrameInTime entity

    set entityStatusIsDirty, (ix + entityStatus)
    call cleanEntitySprite
    ;; Move to left
    ld a, (ix + entityAddress)
    sub 0x01
    ld (ix + entityAddress), a
    ld a, (ix + entityAddress + 1)
    sbc a, 0x00
    ld (ix + entityAddress + 1), a

    ld a, (ix + entityPosX)
    dec (ix + entityPosX)
    cp enemyHeadPositionX - 1
    ret nz

    ld hl, startPosition
    ld a, ixl
    cp (hl)
    ret nz
    inc hl
    ld a, ixh
    cp (hl)
    ret nz

    ld hl, 0x0000
    ld (startPosition), hl

    ret

destroyEnemy:
    set entityStatusIsFinished, (ix + entityStatus)
    res entityStatusIsDestroyed, (ix + entityStatus)

    ret

; IX => Enemy structure
finishEnemy:
    ld hl, stackEnemies
    jp removeEntityFromStack

enemyHeadSpriteData:
enemyHeadSpriteData1:
DB 0x03,0x0C
DB 0x61,0x0E
DB 0x65,0xFF
DB 0x0F,0x0E
DB 0xE1,0x7F
DB 0xE1,0x0E
DB 0x0F,0x6E
DB 0x07,0x0C
enemyHeadSpriteData2:
DB 0x30,0x0C
DB 0x32,0x0E
DB 0x0F,0x7F
DB 0xF0,0x86
DB 0x55,0xF3
DB 0xAA,0xCA
DB 0xF0,0xA6
DB 0x07,0x0C
enemyNinjaSpriteData:
enemyNinjaSpriteData1:
DB 0x03,0x1F
DB 0x11,0x99
DB 0x10,0xBB
DB 0x00,0xEE
DB 0x00,0xCC
DB 0x11,0xCC
DB 0x33,0x66
DB 0x66,0x33
enemyNinjaSpriteData2:
DB 0x0F,0x4C
DB 0x33,0x44
DB 0x31,0x44
DB 0x11,0xCC
DB 0x11,0x88
DB 0x33,0xCC
DB 0x66,0x44
DB 0x33,0x66
enemyUfoSpriteData:
enemyUfoSpriteData1:
DB 0x33,0xCC
DB 0x52,0x4A
DB 0xA5,0xA5
DB 0x55,0x22
DB 0x88,0xAA
DB 0x99,0x11
DB 0x55,0x11
DB 0x44,0xAA
enemyUfoSpriteData2:
DB 0x33,0xCC
DB 0x25,0xA4
DB 0x5A,0x5A
DB 0x55,0x22
DB 0x44,0x99
DB 0x88,0x99
DB 0x88,0xAA
DB 0x55,0x22
