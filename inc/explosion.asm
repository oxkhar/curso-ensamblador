
entityStruct explosion, updateExplosion, 0x00, 0x00, 0x00, explosionSprite, 0x06

spriteStruct explosionSprite, 0x08, 0x01, 0x03, animateSpriteOnce

; HL => Address to draw explosion
createExplosion:
    ld ix, explosion
    res entityStatusIsFinished, (ix + entityStatus)

    ld (ix + entityAddress), l
    ld (ix + entityAddress + 1), h

    ld hl, stackEntities
    call addEntityInStack

    jp spriteInitAnimation

; IX => Explode structure
updateExplosion:
    call spriteUpdate  ; return IY with sprite

    ld a, (ix + spriteFrame)
    cp lastFrame   ; End of animation?
    ret nz

finishExplosion:
    set entityStatusIsFinished, (ix + entityStatus)

    ld hl, stackEntities
    jp removeEntityFromStack

explosionSpriteData:
explosionSprite1:
    DB 0x00
    DB 0x00
    DB 0x00
    DB 0x32
    DB 0x31
    DB 0x00
    DB 0x00
    DB 0x00
explosionSprite2:
    DB 0x90
    DB 0x40
    DB 0x30
    DB 0x73
    DB 0x73
    DB 0x30
    DB 0x50
    DB 0x40
explosionSprite3:
    DB 0x00
    DB 0x90
    DB 0x00
    DB 0x80
    DB 0x44
    DB 0x00
    DB 0x11
    DB 0x90
explosionSprite4:
    DB 0x00
    DB 0x00
    DB 0x00
    DB 0x00
    DB 0x00
    DB 0x00
    DB 0x00
    DB 0x00
