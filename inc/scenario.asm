
scenarioLenght EQU 67

; Define video address positions for diferent elements
;             _name label_    _Horiz.byte_           _Vert.Lines_
defPositions   scenario,         5,                    14
defPositions   roof,           scenarioPositionX,     scenarioPositionY
defPositions   floor,          scenarioPositionX,    (scenarioPositionY + 16)
defPositions   wall,  (scenarioPositionX + scenarioLenght - 1), (scenarioPositionY + 8)

tileStruct roofTile, 0x10

roofTileData:
    DB 0xF0,0x5A,0xA5,0x1E,0x0F,0x0A,0x05,0x08
    DB 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00


tileStruct floorTile, 0x04

floorTileData:
    DB 0xFF,0xA5,0x5A,0xFF
floorTileSolid:
    DB 0xFF,0xFF,0xFF,0xFF

floorPattern:
    DB 0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,1,0,1,1,0,0,0,0,0,1,0,0,-1
floorPattern2:
    DB 0,0,0,1,0,0,1,0,1,1,0,0,0,0,0,1,0,0,-1


