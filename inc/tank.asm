
tankStatusShoting EQU 4

tankReloadTime EQU 50
tankShotSpriteSpeed EQU 0x06

;;
stackEntity Player, 2

tankEntityData EQU entityXtraData
tankReload EQU tankEntityData + 0

entityStruct tank, updateTank, scenarioPositionX,  (scenarioPositionY + 8), 0x00, tankSprite, 0x00
    tankXtraData:
        DB 0x00 ; times Reload

spriteStruct tankSprite, 0x08, 0x02, 0x01, animateNull
spriteStruct tankShotSprite, 0x08, 0x02, 0x04, animateSpriteBounce

; ######
createTank:
    ld ix, tank
    res entityStatusIsFinished, (ix + entityStatus)

    call stopTank

    ld hl, stackPlayer
    jp addEntityInStack

; IX => Tank structure
updateTank:
    bit tankStatusShoting, (ix + entityStatus)
    jr nz, _tankIsShoting

    ld a, (ix + tankReload)
    or 0x00
    jr z, _tankFire

_tankReloading:
    dec (ix + tankReload)
    ret
_tankIsShoting:
    jp spriteUpdate
_tankFire
    call createTankFiring
    jp createShot

; IX => Tank Structure
createTankFiring:
    set tankStatusShoting, (ix + entityStatus)

    ld (ix + entitySprite), tankShotSprite
    ld (ix + spriteSpeed), tankShotSpriteSpeed
    jp spriteInitAnimation

; TODO: IX param
finishTankFiring:
    ld ix, tank
stopTank:
    set entityStatusIsDirty, (ix + entityStatus)
    res tankStatusShoting, (ix + entityStatus)

    ld (ix + tankReload), tankReloadTime ; Reset time to reloloading
    ld (ix + entitySprite), tankSprite
    ld (ix + spriteCurrent), 0x00
    ld (ix + spriteFrame), lastFrame

    ret

tankSpriteData:
tankShotSpriteData:
    DB 0x13, 0x10
    DB 0x30, 0xD6
    DB 0x73, 0xB6
    DB 0x30, 0xC0
    DB 0x01, 0x08
    DB 0x33, 0xEE
    DB 0x76, 0x5A
    DB 0x23, 0xA5
tankShotSpriteData1:
    DB 0x02, 0x20
    DB 0x30, 0xA4
    DB 0x72, 0xE4
    DB 0x30, 0xC0
    DB 0x01, 0x08
    DB 0x33, 0xEE
    DB 0x76, 0x5A
    DB 0x23, 0xA5
tankShotSpriteData2:
    DB 0x04, 0x80
    DB 0x61, 0x80
    DB 0xF5, 0x80
    DB 0x70, 0x00
    DB 0x03, 0x00
    DB 0x77, 0xCC
    DB 0xDE, 0x4A
    DB 0x65, 0xA4
tankShotSpriteData3:
    DB 0x14, 0x00
    DB 0x52, 0x00
    DB 0xF6, 0x00
    DB 0x60, 0x00
    DB 0x02, 0x00
    DB 0x77, 0xCC
    DB 0xED, 0x84
    DB 0x56, 0x48
