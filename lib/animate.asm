
animateFrequencyScreen EQU 50

timer:
    DB 0x00

;;
; A  => Current frame in time
; B => Speed
; C => returns next frame to draw
calculateNextFrameTime:
    add a, b ; Calculate when is the next frame moment to draw
    ld c, a

    sub animateFrequencyScreen
    ret m ; Next frame time is under 50 frames count

    ld c, a ; Reset count frame time over 50 to valid interval

    ret

;;
; IX => Entity structure
; Destroy: A, BC
spriteInitAnimation:
    set entityStatusIsDirty, (ix + entityStatus)

    nextFrameInTime sprite

    ld (ix + spriteCurrent), 0x00
    ld (ix + spriteState), 0x00

    ret

;;
; IX => Entity structure
; Destroy: HL, DE, A
; Returns: IY => Sprite entity
spriteUpdate:
    ld a, (timer)
    cp (ix + spriteFrame) ; Check if sprite frame is in time to draw
    ret nz

    nextFrameInTime sprite

    ld e, (ix + entitySprite)      ;
    ld d, (ix + entitySprite + 1)  ;
    ld iyl, e                      ; IY with sprite structure
    ld iyh, d
    ld l, (iy + spriteAnimation)     ; Animation update to execute
    ld h, (iy + spriteAnimation + 1) ;

    jp (hl)    ; invoke animation process to update next sprite to update

;;;;;; Aimations

;;
; No animation
animateNull:
    ret

;;
;
animateSpriteOnce:
    inc (ix + spriteCurrent)

    ld a, (iy + spriteNumImages)
    cp (ix + spriteCurrent)
    jr z, _animateSpriteOnceEnd
        set entityStatusIsDirty, (ix + entityStatus)
        ret
_animateSpriteOnceEnd:
    ld (ix + spriteFrame), lastFrame
    ld (ix + spriteCurrent), 0x00
    ret
;;
;
animateSpriteLoop:
    inc (ix + spriteCurrent)

    set entityStatusIsDirty, (ix + entityStatus)

    ld a, (iy + spriteNumImages)
    cp (ix + spriteCurrent)
    ret nz

    ld (ix + spriteCurrent), 0x00
    ret
;;
;
spriteAnimateStateStep EQU 1
animateSpriteBounce:
    bit spriteAnimateStateStep, (ix + spriteState)
    jr z, _animateBounceForward
_animateBounceBackward:
        dec (ix + spriteCurrent)
        jp m, _animateBounceFinish
        set entityStatusIsDirty, (ix + entityStatus)
        ret
_animateBounceForward:
        inc (ix + spriteCurrent)
        set entityStatusIsDirty, (ix + entityStatus)
_animateBounce:
    ld a, (iy + spriteNumImages)
    cp (ix + spriteCurrent)
    ret nz

    set spriteAnimateStateStep, (ix + spriteState)
    dec (ix + spriteCurrent)
    ret
_animateBounceFinish:
    ld (ix + spriteCurrent), 0x00
    ld (ix + spriteState), 0x00
    ld (ix + spriteFrame), lastFrame
    ret
