;;
; Stack
stackErasePointer:
    DW 0x0000
stackErase:
    DS StackEntityTotal * 2, stackEnd
    DB stackEnd
    DB stackEnd
;;
; To erase a sprites
; Destroy HL, DE, BC, AF
eraseEntitySprites:
stackLoopBegin stackErase, stackEraseSprites

    ld e, (ix + entitySprite)
    ld d, (ix + entitySprite + 1)
    ld iyl, e
    ld iyh, d

    ld l, (ix + entityEraseAddress)
    ld h, (ix + entityEraseAddress + 1)

    cleanSpriteLogic stackErase

    ld (ix + entityEraseAddress), 0x00
    ld (ix + entityEraseAddress + 1), 0x00

stackLoopEnd stackErase, stackEraseSprites
    ld a, stackEnd
    ld (stackErase), a
    ld (stackErase + 1), a
    ret

;;
; Clean screen area where entity sprite was drawn
; Params: IX => Entity to clean old sprite drew before
; Destroy HL, A
cleanEntitySprite:
    ld a, (ix + entityEraseAddress + 1)
    or 0
    ret nz

    ld l, (ix + entityAddress)
    ld h, (ix + entityAddress + 1)
    ld (ix + entityEraseAddress), l
    ld (ix + entityEraseAddress + 1), h

    ld hl, stackErase
    jp addEntityInDinamycStack

;;
drawPatternAddressIni: DW 0x0000
drawPatternRepeatIni: DB 0x00
drawPatternAddress: DW 0x0000
drawPatternRepeat: DB 0x00
drawPatternRepeatTotal:  DB 0x00
; A  => Repeat pattern through num horizontal bytes
; HL => Video memory to start to draw
; IX => Pattern structure
; IY => Tile struture
drawPattern:
    ld (drawPatternRepeatIni), a
    ld (drawPatternRepeat), a
    xor a
    ld (drawPatternRepeatTotal), a
    ld (drawPatternAddressIni), hl
drawPatternInit:
    ld (drawPatternAddress), hl
_drawPatternFind:
    ld b, (ix)
    ld c, 0
    ld a, -1
    cp b
    jr z, copyPattern
_drawPatternLoop:
    inc c        ; c => num times occurs tile
    inc ix       ; b => Tile to draw
    ld a, (ix)

    cp b
    jr nz, _patternDifferent
    cp -1
    jr nz, _drawPatternLoop
_patternDifferent:
    ld a, (drawPatternRepeat)
    sub c
    jp p, _patternDrawContinue
        add a, c
        ld c, a
        xor a
_patternDrawContinue:
        ld (drawPatternRepeat), a

drawPatternTile:
    ex af, af'
    ld a, (drawPatternRepeatTotal)
    add a, c
    ld (drawPatternRepeatTotal), a

    ; ready to draw B image
    ld a, c
    ld hl, (drawPatternAddress)
    call drawTile

    ex af, af'   ; Num repeats is over
    ret z

    ld hl, drawTileRepeat ; next video address to draw
    ld d, 0
    ld e, (hl)
    ld hl, (drawPatternAddress)
    add hl, de

    jr nz, drawPatternInit

copyPattern:
    ld d, h
    ld e, l
    ld hl, (drawPatternAddressIni)

    ld a, (iy + imageHeight)  ; Draw rest of tiles copy pattern already draw
    ex af, af'
_copyPatternLoop:
    ld a, (drawPatternRepeat)
    ld b, 0  ; Num pattern bytes to copy
    ld c, a  ;

    ldir

    ex af, af'
    dec a
    ret z
    ex af, af'

    ld a, (drawPatternRepeat)  ; Reset destination at begining of next line
    ld b, a
    ld a, e
    sub b
    ld l, a
    ld a, d
    sbc a, 0
    ld h, a
    calculateNextLine patternDest
    ld d, h
    ld e, l

    ld a, (drawPatternRepeatTotal) ; Reset origin at begining of pattern of next line
    ld b, a                        ; (From DE value - lenght of pattern)
    ld a, l
    sub b
    ld l, a
    ld a, h
    sbc a, 0
    ld h, a

    jr _copyPatternLoop

    ret


drawImageVideoAddress: DW 0x0000
drawImageProcess: DW 0x0000
; B   => Num Image to draw in sprite
; HL  => Video memory to start to draw
; HL' => Process address to invoke in main process
; IY  => Sprite structure to draw image
drawImage:
    ld e, (iy + imageAddress)     ; load DE with image address to draw
    ld d, (iy + imageAddress + 1) ;
    ld a, b
    or 0
    jr z, _drawImageMain

    ld c, (iy + imageSize)  ; Calculate address of tile position in DE
    ld a, 0                 ; adding size tile for each position
_drawImagePosition:
        add a, c
        djnz _drawImagePosition
_drawImageCalculate:       ; calculate new address in DE with the position of image
    add a, e
    ld e, a
    ld a, 0
    adc a, d
    ld d, a
_drawImageMain:
    ld c, (iy + imageHeight) ; To iterate foreach line
_drawImageInit:
    ld (drawImageVideoAddress), hl
_drawImageProcessToRun: ; Main process to invoke
    exx
    jp (hl)         ; Here jump to process to draw
drawImageReturn:       ; Process must invoke this to jump next to draw
    ld hl, (drawImageVideoAddress)
    calculateNextLine image

    dec c    ; Height is decremented for each line drawn
    jr nz, _drawImageInit  ; Come on to next line
    ret

;###### To draw a sprite
; B  => Nº Image to draw in Sprite
; HL => Video memory to start to draw
; IY => Sprite struture
; Destroy HL, DE, BC, AF
drawSprite:
    exx
    ld hl, drawSpriteProcess
    exx

    jp drawImage

; USE...
; HL => Video memory to start to draw
; DE => Image start address to draw
; IY => Sprite struture
; Don't touch C, it's needed by drawImageReturn
drawSpriteProcess:
    exx
    ld b, (iy + imageWidth)
_drawSpriteLoop:
    ld a, (de)
    ld (hl), a
    inc de
    inc hl
    djnz _drawSpriteLoop

    jp drawImageReturn

;###### To draw a tile
; Use
drawTileRepeat: DB 0x00
; A  => Repeat tile through num horizontal bytes
; B  => Nº Image to draw in sprite
; HL => Video memory to start to draw
; IY => Sprite struture
; Destroy AF, HL, BC, DE, IY
drawTile:
    ld (drawTileRepeat), a

    exx
    ld hl, drawTileProcess
    exx

    jp drawImage

; USE...
; HL => Video memory to start to draw
; DE => Image start address to draw
; IY => Sprite struture
; Don't touch C, it's needed by drawImageEnd
drawTileProcess:
    exx
    ld a, (drawTileRepeat)
    ld b, a
    ld a, (de)
_drawTileLoop:
        ld (hl), a
        inc hl
        djnz _drawTileLoop

    inc de

    jp drawImageReturn

; Modify HL video address to next line
; HL => Current Address
; Destroy: A
; Returns: HL => New address
nextLineVideoAddress:
    calculateNextLine nextLine
    ret

; Modify HL video address to next line
; HL => Current Address
; Destroy: A
; Returns: HL => New address
previousLineVideoAddress:
    ld a, h      ; [4]
    sub 0x08     ; [7]
    ld h, a      ; [4]
    cp 0xbf      ;
    ret nz       ;
    ld a, l      ;
    sub 0x50  ;
    ld l, a      ;
    ld a, 0xf8   ;
    sbc a, 0     ;
    ld h, a      ;
    ret


;###### To draw a sprite
; HL => Video memory to start to draw
; IY => Sprite struture
; Destroy HL, DE, BC, AF
eraseSprite:
    cleanSpriteLogic ''

    ret

