

PPI_PORT_B EQU 0xf5
; Destroy AF, BC
waitVSync:
    ld b, PPI_PORT_B    ;; PPI port B input
    waiting:
       in a,(c)         ;; [4] read PPI port B input
                        ;; (bit 0 = "1" if vsync is active,
                        ;;  or bit 0 = "0" if vsync is in-active)
        rra             ;; put bit 0 into carry flag
        jp nc, waiting  ;; if carry not set, loop, otherwise continue
    ret

; Wait some time to draw and wait to vertical syncwas reached
; B => Number of times to wait (1 time ~ 0.003 seconds)
; Destroy AF, BC
waitSomeTime:
    halt
    djnz waitSomeTime
    jp waitVSync

RST_INT_ADDRESS EQU 0x0038
; Destroy: HL
disableFirmware:
    di      ;; disable interrupts
    im 1    ;; interrupt mode 1 (call to interrupt handler at &0038)

    ; ld hl, 0x21e5   ; push hl  (0xe5)
    ; ld hl, contador ; ld hl, contador  (0x21, **, **)
    ; ld hl, 0xe134   ; inc (hl)  (0x34)
                      ; pop hl    (0xe1

    ld hl, 0xc9fb   ;; write EI:RET to interrupt handler at &0038
                    ;; 0xc9 corresponds to the opcode RET
                    ;; 0xfb corresponds to the opcode EI
    ld (RST_INT_ADDRESS), hl

    ei       ;; enable interrupts
    ret

;
changeColour:
    ld     b, 0x7F          ;; [2] B = Gate Array Port (0x7F). C has the command that GA will execute.
    out  (c), l             ;; [4] GA command: Select PENR. C = Command + Parameter (PENR + PEN number to select)

    ld     a, 0x40          ;; [2] (CCCnnnnn) Mix 3 bits for INKR command (C) and 5 for INKR number (n).
    or     h                ;; [1]
    out  (c), a             ;; [4] GA command: Set INKR. A = Command + Parameter
                            ;; .... (INKR + INK to be set for selected PEN number)
    ret                     ;; [3] Return

