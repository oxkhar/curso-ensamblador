;; TODO: Optimizar al añadir y borrar con un puntero al siguiente espacio libre
;; TODO: Cuando se borra un elemento traer el último al vacio que queda
;;
; IX => Entity address
; HL => Stack entities to add
; Destroy: A
addEntityInStack:
    ld a, (hl)
    cp stackEmptyGap
    jr nz, _slotNotFoundFirst
    inc hl
    ld a, (hl)
    cp stackEmptyGap
    jr nz, _slotNotFound
_emptySlotFound:
        dec hl
        ld a, ixl
        ld (hl), a
        inc hl
        ld a, ixh
        ld (hl), a
        ret
_slotNotFoundFirst:
    inc hl
_slotNotFound:
    inc hl
    jr addEntityInStack

;;
; IX => Entity address
; HL => Stack entities to add
; Destroy; AF, HL
addEntityInDinamycStack:
    ld a, (hl)
    cp stackEnd
    jr nz, _slotNotFoundFirstDynamic
    inc hl
    ld a, (hl)
    cp stackEnd
    jr nz, _slotNotFoundDynamic
_emptySlotFoundDynamic:
        dec hl
        ld a, ixl
        ld (hl), a
        inc hl
        ld a, ixh
        ld (hl), a
        inc hl
        ld (hl), stackEnd
        inc hl
        ld (hl), stackEnd
        ret
_slotNotFoundFirstDynamic:
    inc hl
_slotNotFoundDynamic:
    inc hl
    jr addEntityInDinamycStack

;;
; IX => Entity address
; HL => Stack entities to add
; Destroy: DE, AF, HL,
removeEntityFromStack:
    ld d, h
    ld e, l

    ld a, (hl)
    cp ixl
    jr nz, _entitySlotLowNotFound

    inc hl
    ld a, (hl)
    cp ixh
    jr nz, _entitySlotNotFound
_entitySlotFound:
    ex de, hl
    ld (hl), stackEmptyGap
    inc hl
    ld (hl), stackEmptyGap

    jp cleanEntitySprite ;; Clean sprite from screen

_entitySlotLowNotFound:
    inc hl
_entitySlotNotFound:
    inc hl
    jr removeEntityFromStack
