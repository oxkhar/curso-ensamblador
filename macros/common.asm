
true EQU 0xff
false EQU 0x00
lastFrame EQU 0xff

;;
; Modify HL video address to next line
; HL => Current Address
; Destroy: A, HL
calculateNextLine MACRO _exitLabel
    ld a, 0x08   ; [7]
    add a, h     ; [4]
    ld h, a      ; [4]
    jp nc, _exitLabel ## NextLineExit ; [10] (Better => 25)
    ld a, l      ; [4]
    add a,0x50   ; [7]
    ld l, a      ; [4]
    ld a, 0xC0   ; [7]
    adc a, h     ; [4]
    ld h, a      ; [4] (Worst => 45)
_exitLabel ## NextLineExit :
ENDM

;;;; DEBUG ;;;;;

DEBUG DEFL false

colorBorder MACRO _color, _numHalts
IF ! DEBUG
    EXITM
ENDIF

    colourParam DEFL 0x0010 + _color * 256

    ld hl, colourParam
    call changeColour

IF ! NUL _numHalts
    REPT _numHalts
    halt
    ENDM
ENDIF
ENDM

counterInt MACRO
    xor a
    ld (contador), a

    ld a, (contador)
    cp 0
    jr z, kk
    ld b, a
    ld hl, 0xc008
_contador:
    ld (hl), 0x3c
    inc hl
    djnz _contador
kk:
    jr kk
ENDM
