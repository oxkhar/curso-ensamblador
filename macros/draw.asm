
;;
; Params: IX => Entity structure
; Destroy: B, A
nextFrameInTime MACRO _fieldType
    ld a, (timer)
    add a, (ix + _fieldType ## Speed) ; Calculate when is the next frame moment to draw
    ld b, a
    sub animateFrequencyScreen
    jp m, $ + 4 ; Next frame time is under 50 frames count
        ld b, a ; Reset count frame time over 50 to valid interval
    ld (ix + _fieldType ## Frame), b
ENDM

;;
;###### To draw a sprite
; HL => Video memory to start to erase
; IY => Sprite struture
; Destroy HL, DE, BC, AF
cleanSpriteLogic MACRO _nameLogic
    ld a, (iy + imageHeight)
    ld c, (iy + imageWidth)

_eraseSpriteImageInit ## _nameLogic:
    ld b, c
    ld e, l
    ld d, h
_eraseSpriteLoop ## _nameLogic:
    ld (hl), 0x00
    inc hl
    djnz _eraseSpriteLoop ## _nameLogic

    ex de, hl
    ex af, af'
    calculateNextLine eraseImage ## _nameLogic
    ex af, af'

    dec a    ; Height is decremented for each line drawn
    jr nz, _eraseSpriteImageInit ## _nameLogic  ; Came on to next line
ENDM
