
StackEntityTotal DEFL 0

stackEntity MACRO _stackEntity, _numElements

StackEntityTotal DEFL StackEntityTotal + _numElements

;;
; Stack
stack ## _stackEntity ## Pointer:
    DW 0x0000
stack ## _stackEntity:
    DS _numElements * 2
    DW 0xffff

;;
; Update Entities
; Destroy: HL, DE, AF
update ## _stackEntity:
stackLoopBegin stack ## _stackEntity, updateStack ## _stackEntity
stackLoopSwapStackPointer stack ## _stackEntity

    call callUpdate ## _stackEntity

stackLoopSwapStackPointer stack ## _stackEntity
stackLoopEnd stack ## _stackEntity, updateStack ## _stackEntity
    ret

callUpdate ## _stackEntity:
    ld l, (ix + entityUpdateProcess)
    ld h, (ix + entityUpdateProcess + 1)
    jp (hl)

;;
; Draw Entities
; Destroy: HL, DE, AF
draw ## _stackEntity:
stackLoopBegin stack ## _stackEntity, drawStack ## _stackEntity
    ; FIXME: Remove!, always draw although entity will be disabled
    ; bit entityStatusIsFinished, (ix + entityStatus)
    ; jr nz, _drawStack ## _stackEntity ## Loop

    bit entityStatusIsDirty, (ix + entityStatus)
    jr z, _drawStack ## _stackEntity ## Loop
    res entityStatusIsDirty, (ix + entityStatus)

    ld e, (ix + entitySprite)
    ld d, (ix + entitySprite + 1)
    ld iyl, e
    ld iyh, d

stackLoopSwapStackPointer stack ## _stackEntity

    ld l, (ix + entityAddress)
    ld h, (ix + entityAddress + 1)
    ld b, (ix + spriteCurrent)
    call drawSprite

stackLoopSwapStackPointer stack ## _stackEntity
stackLoopEnd stack ## _stackEntity, drawStack ## _stackEntity
    ret

ENDM
