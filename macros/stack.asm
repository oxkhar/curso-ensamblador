
stackEnd EQU 0xff
stackEmptyGap EQU 0x00

;;
; Destroy => IX, A
stackLoopBegin MACRO _nameStack, _nameLoop
_nameLoop:
    ld ( _nameStack ## Pointer ), sp
    ld sp, _nameStack

_ ## _nameLoop ## Loop:
    pop ix

    ld a, ixl
    cp ixh
    jr nz, _ ## _nameLoop ## LoopBegin
    cp lastFrame
    jr z, _ ## _nameLoop ## Exit
    cp stackEmptyGap
    jr z, _ ## _nameLoop ## Loop
_ ## _nameLoop ## LoopBegin:
ENDM

;;
; Swap between stack run program and stack entity address
; Destroy => HL'
stackLoopSwapStackPointer MACRO _nameStack
    exx
    ld hl, ( _nameStack ## Pointer )
    ld ( _nameStack ## Pointer ), sp
    ld sp, hl
    exx
ENDM

;;
;
stackLoopEnd MACRO _nameStack, _nameLoop
    jr _ ## _nameLoop ## Loop
_ ## _nameLoop ## Exit:
    stackLoopSwapStackPointer _nameStack
_ ## _nameLoop ## Return:
ENDM

