
;;
; Entities
entityStatusIsDirty EQU 1
entityStatusIsFinished EQU 2
entityStatusIsDestroyed EQU 3

entityAddress EQU 0
entityPosX EQU 2
entityPosY EQU 3
entityStatus EQU 4
entitySpeed EQU 5
entityFrame EQU 6

entitySprite EQU 7
spriteCurrent EQU 9
spriteState EQU 10
spriteSpeed EQU 11
spriteFrame EQU 12

entityEraseAddress EQU 13
entityUpdateProcess EQU 15

entityXtraData EQU 17
entitySize EQU 17

entityStruct MACRO _name, _update, _x, _y, _speed, _spriteRef, _spriteSpeed

defPositions _name, _x, _y

_name:
    DW _name ## Position ; Address to draw
    DB _x       ; X position in screen (in bytes max. 80)
    DB _y       ; Y position in screen (in lines max. 200)
    DB 0x00     ; Status

    DB _speed       ; Move speed (1 - 50)
    DB lastFrame    ; Frame when draw

    DW _spriteRef   ; Sprite to use to draw
    DB 0x00         ; Num. image to draw
    DB 0x00         ; State in animation (step forward or resverse,...)
    DB _spriteSpeed ; Sprite Speed
    DB lastFrame    ; Frame to draw sprite (0xff not draw)

    DW 0x0000   ; Address to erase previous draw
    DW _update  ; Update process to execute

ENDM

;;
; Image
imageAddress EQU 0
imageHeight EQU 2
imageWidth EQU 3
imageSize EQU 4

imageStruct MACRO _img, _h, _w

    DW _img    ; Image address
    DB _h      ; Height
    DB _w      ; Width
    DB _w * _h ; Size

ENDM

;;
; Sprite
spriteImage EQU 0
spriteAnimation EQU 5
spriteNumImages EQU 7

spriteStruct MACRO _name, _h, _w, _total, _animation
_name:
_name ## SpriteImage:
    imageStruct _name ## Data, _h, _w  ; Data for sprite, identified with sprite name +  "Data" sufix
_name ## SpriteAnimation:
    DW _animation ; Animation logic (see animation.asm)
    DB _total     ; Num images animation has

ENDM

;;
; Tile images
tileImage EQU 0

tileStruct MACRO _name, _h
_name:
_name ## TileImage:
    imageStruct _name ## Data, _h, 0x01  ; Data for sprite, identified with sprite name +  "Data" sufix

ENDM

;;
; Positions
defPositions MACRO _name, _x, _y
    _name ## Position  EQU (0xC000 + _x + (0x0800 * (_y % 8) + 0x50 * ( _y / 8)))
    _name ## PositionX EQU _x
    _name ## PositionY EQU _y
ENDM
